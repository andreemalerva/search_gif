<div>
    <h1>Buscador de GIF</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/search_gif), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 2283530727
```

# Acerca del proyecto

Este es un proyecto realizado con el framework de React, implementado un buscador de GIF, haciendo la peticion a la API de GIPHY.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://search-gif-am.netlify.app/)🫶🏻

# Deploy a netlify
    - Realicé la construcción del proyecto con `npm run build`
    - Realice la sincronizacion del repositorio en el sitio de netlify
    - Instale: `npm install netlify-cli -g` (Si solicita acceso solo agregar al principio `sudo`)
    - Hice el deploy con: `netlify deploy`
    - Posteriormente vi el sitio en producción con `netlify deploy --prod`

Para más información comparto documentación: [Documentación Netlify](https://docs.netlify.com/site-deploys/create-deploys/) y en [Create React App](https://create-react-app.dev/docs/deployment/)
# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2023 LYAM ANDREE CRUZ MALERVA


## Scripts para correr React y todo eso que quizá quieras saber...

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
