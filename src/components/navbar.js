import '../assets/styles/navbar.css';
import logoAndree from '../assets/images/andreemalerva-logo.png';

function Navbar() {
  return (
    <div className="Navbar bg-glass">
        <div className='container '>
            <nav className="navbar navbar-expand-lg">
                <a href="https://andreemalerva.com/" className="navbar-brand d-flex flex-row align-items-center text-nav">
                    <img src={logoAndree} className="mr-5" width="35" alt="andree logo"/> Andree Malerva
                </a>
            </nav>
        </div>
    </div>
  );
}

export default Navbar;
