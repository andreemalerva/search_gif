const TextList = (props) => {
    const items = props.gifs.map((itemData) => {
    //const items = props.tam.map((itemData) => {
      return <Item url={itemData.images.preview_gif.url} data={props.gifs} />;
    });

    return <div className="wrapper container-sm">{items}</div>;
};

const Item = (props) => {
  return <div className="gif-item"><img src={props.url} alt="prop" /></div>;
};

export default TextList;