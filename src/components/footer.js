import '../assets/styles/footer.css';

function Footer() {
    return (
        <footer className="footer">
            <div className='container '>
                <div className="row">
                    <div className="col-sm padBottom">
                        <h2>Tecnologías usadas:</h2>
                        <h3>HTML, CSS y React js 🛠</h3>
                    </div>
                    <div className="col-sm padBottom">
                        <h2>Este proyecto fue realizado  por:</h2>
                        <h3>Andree Malerva ❤️</h3>
                    </div>
                    <div className="col-sm padBottom">
                        <h2>¡Invitame un café!</h2>
                        <a href="https://www.buymeacoffee.com/andreemalerva"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=andreemalerva&button_colour=FFDD00&font_colour=000000&font_family=Cookie&outline_colour=000000&coffee_colour=ffffff" /></a>
                    </div>
                </div>

            </div>
        </footer>
    );
}
export default Footer;
