import '../assets/styles/notResult.css';
import imageNotFound from '../assets/images/mandalorian.png';

const NotResult = (props) => {
    if (!props.isDatos) {
        return null
    }
    return (
        <div className="notResult">
            <div className='container box-notResult'>
                <p>No encuentro la palabra que buscaste, intenta con otra.</p>
                <img src={imageNotFound}></img>
            </div>
        </div>
    )
}

export default NotResult;