import '../assets/styles/card.css';
import iconSearch from '../assets/images/search.svg';
import { GiphyFetch } from '@giphy/js-fetch-api'
import { useState } from 'react'
import TextList from './TextList'
import Error from './Error'
import NotResult from './NotResult'

const giphy = new GiphyFetch('lDd2BBOGirpuAij3ts67hjjz7Wq5ljUt')

function Card() {
  const [text, setText] = useState('')
  const [results, setResults] = useState([])
  const [err, setErr] = useState(false);
  const [datos, setDatos] = useState(false)
  const handleInput = (e) => {
    setText(e.target.value)
  }

  const handleSubmit = (e) => {
    if (text.length === 0) {
      //set error state to true
      setErr(true)
      return
    }
    const apiCall = async () => {
      const res = await giphy.search(text, { sort: 'relevant', lang: 'es', limit: 12, offset: '25', rating: 'g', type: 'gifs' })
      console.log(res.data);
      setResults(res.data)
      const datosF = res.data;
      if (datosF.length === 0) {
        setDatos(true)
      }
    }
    apiCall()
    setText('')
    setErr(false)

  }

  return (
    <div className="Card">
      <div className='container body-size'>
        <div className="card">
          <div className="card-header">
            Buscador de GIF
          </div>
          <div className="card-body">
            <h5 className="card-title">Acá puedes encontrar Gifs chidos 😎, solo ingresa una palabra</h5>
            <div className="row">
              <div className='col-sm-9'>
                <input type='text' value={text} onChange={handleInput} placeholder='Buscar GIF'></input>
              </div>
              <div className='col-sm-3'>
                <button onClick={handleSubmit}> <img src={iconSearch} alt="search"></img>Buscar</button>
              </div>
            </div>
          </div>
        </div>
          <Error isError={err} text='Ingresa una palabra' />
          {results.length > 0 && <TextList gifs={results} />}
          <NotResult isDatos={datos} />
      </div>
    </div>
  );
}

export default Card;
